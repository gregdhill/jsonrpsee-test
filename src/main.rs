use runtime::{
    pallets::btc_relay::StoreMainChainHeaderEvent, substrate_subxt::PairSigner, BtcRelayPallet,
    PolkaBtcProvider, PolkaBtcRuntime, RawBlockHeader,
};
use sp_keyring::AccountKeyring;
use std::sync::Arc;

#[tokio::main]
async fn main() {
    env_logger::init();

    let signer1 = PairSigner::<PolkaBtcRuntime, _>::new(AccountKeyring::Alice.pair());
    let provider1 = PolkaBtcProvider::from_url("ws://127.0.0.1:9944".to_string(), signer1)
        .await
        .unwrap();

    let provider1_1 = Arc::new(provider1);
    let provider1_2 = provider1_1.clone();

    let signer2 = PairSigner::<PolkaBtcRuntime, _>::new(AccountKeyring::Bob.pair());
    let provider2 = PolkaBtcProvider::from_url("ws://127.0.0.1:9944".to_string(), signer2)
        .await
        .unwrap();

    // provider
    //     .initialize_btc_relay(RawBlockHeader::from_bytes(&[0; 80]).unwrap(), 0)
    //     .await
    //     .unwrap();

    let mut count = 0;
    loop {
        println!("Running {}", count);
        count += 1;
        tokio::try_join!(
            provider1_1.on_event::<StoreMainChainHeaderEvent<PolkaBtcRuntime>, _, _, _>(
                |event| async move {
                    println!("Event: {:?}", event);
                },
                |err| {
                    println!("Error: {:?}", err);
                },
            ),
            provider1_2.store_block_header(RawBlockHeader::from_bytes(&[0; 80]).unwrap()),
            provider2.store_block_header(RawBlockHeader::from_bytes(&[0; 80]).unwrap()),
        );
    }
}
