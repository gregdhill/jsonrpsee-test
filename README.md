# No such variant in enum Phase

## Terminal 1

```shell
git clone git@gitlab.com:interlay/btc-parachain.git
cd btc-parachain
git checkout dev
cargo run --release --dev
```

## Terminal 2

```shell
git clone git@gitlab.com:interlay/jsonrpsee-test.git
cd jsonrpsee-test
cargo run
```